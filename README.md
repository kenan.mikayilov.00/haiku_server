# Haiku_server


Haiku server is a project that is done for UFAZ Operating System Architecture subject evaluation.
This project consists of 3 versions:
 
 - Version 1:

  In the version 1, there are 2 programs: client and server. Client randomly chooses signal from (SIGINT and SIGQUIT) and sends them to the server. Server waits for signals and then prints the haiku category according to signal.
  SIGINT --> japanese and SIGQUIT --> Western


  - Version 2:

  In this version, the message queue was implemented, and there are 2 programs: reader and writer. Writer takes the lines of haiku from the files created and sends them via message queue, message type 1 for japanese haikus and message type 2 for western haikus. And then reader should enter number (1 or 2) to specify which category haiku he wants to read the haikus. When message queue is empty, the user should run writer again to send the messages to the message queue.

  - Version 3:

  In the last version, the 2 previous versions were merged. There are client and server programs. Client sends signals to the server, and server reads the messages of 2 categories depending on the signal, if there is no messages in the queue, program itself refills the message queue, and when user terminates the client program by clicking CTRL+Z, the server terminates itself also.




